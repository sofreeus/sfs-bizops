# Business Maintenance

## Yearly

0000-00-00 something something strategy

## Quarterly

0000-00-00 something something strategy

## Monthly

The monthly Board Meeting is currently on the Tuesday after the 1st Saturday

0000-00-00 Close, Retro, Plan

Close the last month
- Review recent Closed - Is there anything to share or are any follow-up tickets needed?
- Review all Doing - Is it done? Is it blocked? What's needed? Clean out the Doing column to the left or right, but not stay.
- Review all ToDo - Is is needed/wanted this month? If no, Backlog. Is it ready to work? Is this column prioritized?
- Reivew some Backlog - If we're never going to do it, toss it.

Retro and choose a Kaizen
- Every participant says one thing that went well and one thing that went poorly
- Participants may also offer shout-outs, questions, and suggestions
- One improvement to business-as-usual, a "kaizen", should be chosen and implemented

Plan the current month
- Ensure that important issues have a lead and at least one helper
- Ensure that nobody has more than they can handle this month

0000-00-00 Ensure that the next three SFFSs/Gatherings are focused on building community and have Plan tickets and that they are assigned

0000-00-00 Ensure that the next three Board Meetings are (correctly) scheduled on Meetup and calendar

0000-00-00 Ensure that the next three third Saturday events are focused on building capital and have Plan tickets and that they are assigned

## Weekly

2024-04-02 As a Team, prune stale branches and process merge requests. Choose a driver randomly from those who have driven the least.

2024-04-02 We have a weekly "standup" at 7PM on Tuesday. Even weeks, we meet online. Odd weeks, we write in. Every week, the format is:
- What have I done lately that the team should be aware of?
- What will I do this week?
- What impedes me? Especially, what can others on my team help with?
