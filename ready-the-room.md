# How To Ready The Room
- Clean the room
- Point-of-presence
	* Big screen and sound
	* Open BBB URL
	* Share webcam
	* Share screen
	* Share a document
- Distribute schwag piles
  * hex pad
  * karma envelope
  * sticky notes
  * stickers
  * pen/pencil
  * chapstick
- Miscellaneous
  * Tables and chairs
  * Power: extension cords, power strips
  * 
  * Network stuph: switch, cables, wireless
  * Whiteboard
