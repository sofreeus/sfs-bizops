# Hot Links

- [Mattermost](https://mm.sofree.us)

# Cool Links

- [Email List](http://lists.sofree.us/cgi-bin/mailman/listinfo/sfs)
- [Meetup](https://www.meetup.com/sofreeus/)
- [GitLab.com](https://gitlab.com/sofreeus)
- [YouTube](https://www.youtube.com/sofreeus)
- [www.sofree.us](https://sofree.us)
- [LinkedIn](https://www.linkedin.com/company/76784851/admin/dashboard/)

# Places

- The Comfy Room: 4237 W Grand Ave, Denver 80123

# Board Members

- [Rich Glazier](https://www.linkedin.com/in/erglaz/)
- [Aaron](mailto:Aayore@SoFree.US)
- [Heather](mailto:HLWillson@SoFree.US)
- [David L. Willson](https://gitlab.com/DLWillson)
  * phone  720-333-LANS
