# How to Prep and Deliver a SiB Session

A Small is Beautiful (SiB) session is your chance to share something cool about free software with friends. It's meant to be casual, fun, and useful.

## The Only Real Rules
- Keep it around 22 minutes (definitely under an hour)
- Focus on free software, not proprietary/unfree software
- That's it!

## Choose Your Style
Pick what works for you:
- Talk through some slides
- Show us how something works (live demo)
- Guide us through trying something (hands-on)
- Lead a discussion
- Run a game or exercise
- Mix and match the above

## Tips for Success
- Pick a topic you care about
- Start with the basics - we're all beginners at something
- Leave time for questions
- Test your demos beforehand
- If things break, no worries - troubleshooting is learning too!

## Before Your Session
- Let us know what (if anything) folks should bring or install
- Test your tech if you're using any
- Show up a few minutes early to get set up

## During Your Session
- Tell us what we'll learn
- Keep it simple
- Share any helpful links or commands in the chat
- Have fun - if you're enjoying it, we probably are too!

## Need Ideas?
- Show us a tool you use
- Walk through solving a problem
- Explain a concept you recently learned
- Share a discovery that made your life easier

Remember: You don't need to be an expert. If you learned something useful, others will probably find it useful too!

Questions? Ask in Mattermost or talk to any SFS regular.
