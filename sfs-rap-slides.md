---
theme: gaia
backgroundColor: white
---
![bg fit](https://gitlab.com/sofreeus/sfs-bizops/-/raw/master/media/SFS%20Logo.png)

---
# Welcome and Purpose

* Welcome to the Software Freedom School
* SFS exists to teach the world how how to use and why to choose Free Software
* We are gathered here now for that purpose

---
# Free Software Definition

Free Software is software that you are free to:
* **Use** for whatever purpose you see fit, including reverse engineering.
* **Study** how it works, for which access to the source code is a
  pre-requisite.
* **Copy** it to share it with your friends.
* **Modify** it to better suit your needs.

---
# Why Choose Free Software

We guess that the making and use of software is art and/or science and that science and art together are human progress. It seems important to leave humans free to make progress.

Free Software authors leave us free to progress from dependent consumer to interdependent co-creators, so we choose their products over those of others.

---
# Moral Consistency

* SFS benefits from Free Software individually and corporately
* Everything SFS makes is free:
  * Code is released under GPL
  * Artistic works are CC BY SA
* We give a share upstream

---
# Libre Not Gratis 

* SFS class materials are free-libre like Free Software.
* Class deliveries are *not* free-gratis. 
* SFS only sells class deliveries. If it doesn't make money on class
  deliveries, it doesn't make money.

---
# Payment Options

The ask for today's class is: ___ LTC/BTC/USD

* You may pay the ask at https://paypal.me/sfs303
* Or Pay What You Choose: more, less, or differently.

Common ways to Pay What You Choose are:
* Pay with **crypto-currency**
* Take a **Karma Envelope** and send us a payment or a postcard
* **Teach** a class. This is our favorite, and you can make money at it!

If you PWYC, send an email to pwyc@sofree.us so we know what to look for.

---
# Practicalities 

* Restrooms -- Up stairs and to the right
* Refreshments -- On the table
* Recycling -- In the hall outside

---
# Upcoming Events

| Who | What | When | Where |
| --- | ---- | ---- | ----- |
| Telfer, Willson, and all | Gathering: OpenStack and The GLADOS Line | Mar 01 | Online |
| Stone Sanders | Wheel: Linux AGHI | Mar 15 | ??? |
| Andrew Owen, David Willson, and all of us | Gathering: Neovim and The GLADOS Line(?) | Apr 05 | Rule 4 in Boulder |
| ??? | Wheel: Productivity Tools | Apr 19 | ??? |
| all | Gathering | May 3 | Online |
| ??? | Wheel: Programming with Python | May 3 | ??? |
| all | Gathering | July 5 | Online |

---
# We Take Requests!

SFS has delivered several custom classes on request. If you or your employer would like to commission a custom class, in-person, on-site, or remote, just let us know. 

If it's Free Software, we teach it.

---
# Wireless Information

    SSID: SFS-WLAN
    Pass: Freedom-13

---
![bg fit](https://gitlab.com/sofreeus/sfs-bizops/-/raw/master/media/SFS%20Logo.png)
