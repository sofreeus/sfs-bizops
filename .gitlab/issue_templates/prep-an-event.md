# Prepare to Deliver an SFS Event

A "time unit" in this doc is the amount of time it takes to completely deliver the planned material including time reserved for retrieval practice by lab and/or quiz.

SFS has produced learning events of 22 minutes (SIB), 1 hour (LOL), 4 hours (AGHI), 1 day (Intro), and 4 days (Camp).

Recommended Prep and Deliver Plan:

1. Write new material or study existing material for about one time unit.
2. Rehearse the delivery with a couple rubber ducks posing as learners.
    * This should naturally take one time unit.
    * When it is the ducks' time to practice and retrieve what you've taught, put yourself in their position.
    * If you have a lab, do it 'as a duck' on a completely different machine.
    * If you have a quiz, try to answer from what you remember having shown and told the learners.
3. Spend about one time unit continuing to write or study and improve the material.
4. Rehearse the delivery with two friends who are good at active listening and who represent the audience you expect.
    * Make sure to reserve time for your friends to "do the lab", to practice retrieval.
5. Spend one last time unit repairing problems and improving features. This is a bad time to add new features.

Recommended Quality Checks:

* [ ] Ensure that your timing is right. Did you successfully limit your talk time? Did you reserve time for learners to practice retrieval?
* [ ] Ensure that the demo (teacher do) and pair time (learners do) environments are identical, or carefully consider how any differences between them may affect learner success.
* [ ] Ensure that labs are pair and there is time to share.
* [ ] If your class is online, ensure that you are comfortable with Big Blue Button.
