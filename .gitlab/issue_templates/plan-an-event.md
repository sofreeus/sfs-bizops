Assignee, you are the Planner. You may do this alone, but you probably should not. If you are a novice you should ask for help from an expert. If you are an expert, you should bring a novice along for the ride.

But, you are personally responsible for getting it done in a timely fashion. You must **plan** the event far enough in advance that someone can **post** the event far enough in advance that learners have time to plan their participation, and that teachers can **prep** their delivery.

First, you must settle who, what, where, and when.

- [ ] **Who** will teach or facilitate? Tag the teachers if they have GitLab.com accounts.
- [ ] **What** will be learned? Link the materials, if possible.
- [ ] **Where** will the learning event happen? Will it be in-person, hybrid, or online? Addresses and URLs.
- [ ] **When** will the learning event happen? Remember to include time-zone information.

If the event you are planning is a Gathering, [read the additional notes about that](/gathering.md), and ensure that you have *two* in who and what.

---

When you have all four settled:

- [ ] Create a 'post' ticket using the "Post an event" template. Assign it to whoever will 'post' the event. The title of the 'post' ticket should be something like: `Post 4/20: Shoup at The Comfy Room on ZickleTwister` or `Post 4/20: ZickleTwister by Shoup at The Comfy Room`
- [ ] Ask the teachers if they would like 'prep' tickets. If they do, create 'prep' tickets using the "Prep an event" template, and assign them to the teachers who wanted them.
- [ ] If the event will be in-person, ask Heather if she wants a pfood ticket. If so, create a 'pfood' ticket and assign it to her.
- [ ] Create a "payout" ticket using the `payout-an-event` template and assign it to Heather.

After those tickets have been created, planning is complete and this Issue can be dragged to the Done column.
