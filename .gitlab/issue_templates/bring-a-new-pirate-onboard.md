TODO: Consider how this process is affected by different roles: cook, captain, deckhand, etc.

* [ ] Gandi.net
* [ ] GitLab.com
* [ ] Linkedin
* [ ] Mattermost
* [ ] Meetup
* [ ] Nextcloud
* [ ] Proxmox
* [ ] Zimbra

* [ ] Git local customs (push to branch & pr vs push to master)
* [ ] Value-stream briefing: what we mean by Plan, Promote, Prep, etc...
