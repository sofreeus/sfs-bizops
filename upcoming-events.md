# Upcoming Events

Would you rather view the Upcoming Events in your browser? [Click here](https://gitlab.com/sofreeus/sofreeus/blob/master/upcoming-events.md)!

* Saturday, Dec 11: [M is for Make with Dr. Jeffrey S. Haemer](https://www.meetup.com/sofreeus/events/282539249/)
* Sometime between Dec 12 and Dec 18 - N is for Node?
* Sometime between Jan 02 and Jan 08 - O is for Observability / OpenTelemetry with Alex Wise
* Sometime between Jan 09 and Jan 15 - P is for Prometheus, Grafana, and friends with Mike Shoup and Justin Lang
* Sometime between Jan 16 and Jan 22 - Q is for Query with Chris Fedde
* Sometime between Jan 23 and Jan 29 - R is for Ruby with Megan Ruggiero
* Sometime between Jan 30 and Feb 05 - S is for Secure Shell, SCP, SFTP, sshuttle, and rsync
* All day Saturday, February 19th - Cloud Computing Fundamentals (all day)

## Reminders

* We need your help: Tell your friends about our classes! Participate! Teach!
* If you don't see what you want, send a wish to captains@sofree.us!
* We are lifelong learners. Please share learning opportunities, even if they're not produced by SFS.
* We are friends. You can post just about anything to the [ML](http://lists.sofree.us/cgi-bin/mailman/listinfo/sfs). If it's not free-libre, put "OT:" in the subject.
* We are professionals. Your "help wanted" and "job wanted" posts are welcome.
* Come hang out with us in [Mattermost](https://mattermost.sofree.us/sfs303/channels/town-square)!
